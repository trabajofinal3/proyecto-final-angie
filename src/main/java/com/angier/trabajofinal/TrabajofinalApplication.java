package com.angier.trabajofinal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;
import io.micrometer.common.lang.NonNull;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.Data;
import lombok.RequiredArgsConstructor;



@SpringBootApplication
public class TrabajofinalApplication {

	public static void main(String[] args) {
		SpringApplication.run(TrabajofinalApplication.class, args);
	}
	

	@Bean
	org.springframework.boot.CommandLineRunner Runner(EmpleadosRepository EmpleadosRepository){
		return args-> {		
	
		};		
	}
}



@RequiredArgsConstructor
@Data
@Entity
class Empleados{
	@Id
	@GeneratedValue
	private Integer id;

	@NonNull
	private String name;
	@NonNull
	private String lastname;
	@NonNull
	private String Cargo;
	
}


@RepositoryRestResource(path = "Empleados", collectionResourceRel = "Empleados")
@Repository
interface EmpleadosRepository extends JpaRepository<Empleados, Integer> {
}